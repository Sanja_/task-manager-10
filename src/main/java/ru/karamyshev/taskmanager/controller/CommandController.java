package ru.karamyshev.taskmanager.controller;

import ru.karamyshev.taskmanager.api.ICommandController;
import ru.karamyshev.taskmanager.api.ICommandService;
import ru.karamyshev.taskmanager.model.TerminalCommand;
import ru.karamyshev.taskmanager.util.NumberUtil;

public class CommandController implements ICommandController {

    private ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showVersion() {
        System.out.println("\n [VERSION]");
        System.out.println("1.0.8");
    }

    public void showAbout() {
        System.out.println("\n [ABOUT]");
        System.out.println("NAME: Alexander Karamyshev");
        System.out.println("EMAIL: sanja_19.96@mail.ru");
    }

    public void showHelp() {
        System.out.println("\n [HELP]");
        final TerminalCommand[] commands = commandService.getTerminalCommands();
        for (final TerminalCommand command : commands) System.out.println(command);
    }

    public void showCommands() {
        System.out.println("\n [COMMANDS]");
        final String[] commands = commandService.getCommands();
        for (final String command : commands) System.out.println(command);
    }

    public void showArguments() {
        System.out.println("\n [ARGUMENTS]");
        final String[] arguments = commandService.getArgs();
        for (final String argument : arguments) System.out.println(argument);
    }

    public void exit() {
        System.exit(0);
    }

    public void showInfo() {
        System.out.println("\n [INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM (bytes): " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM (bytes): " + usedMemoryFormat);
    }
}
