package ru.karamyshev.taskmanager.service;

import ru.karamyshev.taskmanager.api.ICommandRepository;
import ru.karamyshev.taskmanager.api.ICommandService;
import ru.karamyshev.taskmanager.model.TerminalCommand;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }
}
