package ru.karamyshev.taskmanager.bootstrap;

import ru.karamyshev.taskmanager.api.ICommandController;
import ru.karamyshev.taskmanager.api.ICommandRepository;
import ru.karamyshev.taskmanager.api.ICommandService;
import ru.karamyshev.taskmanager.constant.ArgumentConst;
import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.constant.TerminalConst;
import ru.karamyshev.taskmanager.controller.CommandController;
import ru.karamyshev.taskmanager.repository.CommandRepository;
import ru.karamyshev.taskmanager.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
        inputCommand();
    }

    private void inputCommand() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            parsCommand(command);
        }
    }

    private void parsCommand(final String args) {
        validateArgs(args);
        String[] command = args.trim().split("\\s+");
        for (String arg : command) {
            chooseResponsCommand(arg);
        }
    }

    private void parsArgs(final String... args) {
        validateArgs(args);
        for (String arg : args) {
            chooseResponsArg(arg.trim());
        }
    }

    private void validateArgs(final String... args) {
        if (args != null || args.length > 0) {
            return;
        }
        System.out.println(MsgCommandConst.COMMAND_ABSENT);
    }

    private void chooseResponsArg(String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT: commandController.showAbout(); break;
            case ArgumentConst.VERSION: commandController.showVersion(); break;
            case ArgumentConst.HELP: commandController.showHelp(); break;
            case ArgumentConst.INFO: commandController.showInfo(); break;
            case ArgumentConst.ARGUMENTS: commandController.showArguments(); break;
            case ArgumentConst.COMMANDS: commandController.showCommands(); break;
            default: System.out.println(MsgCommandConst.ARGS_N_FOUND);
        }
    }

    private void chooseResponsCommand(String command) {
        switch (command) {
            case TerminalConst.ABOUT: commandController.showAbout(); break;
            case TerminalConst.VERSION: commandController.showVersion(); break;
            case TerminalConst.HELP: commandController.showHelp(); break;
            case TerminalConst.INFO: commandController.showInfo(); break;
            case TerminalConst.EXIT: commandController.exit(); break;
            case TerminalConst.ARGUMENTS: commandController.showArguments(); break;
            case TerminalConst.COMMANDS: commandController.showCommands(); break;
            default: System.out.println(MsgCommandConst.COMMAND_N_FOUND);
        }
    }
}
