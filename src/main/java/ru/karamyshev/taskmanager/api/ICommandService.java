package ru.karamyshev.taskmanager.api;

import ru.karamyshev.taskmanager.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();
}
